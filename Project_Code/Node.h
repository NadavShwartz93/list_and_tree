#ifndef _NODE_
#define _NODE_

struct Node
{
	int val;
	Node* next;
};

#endif // !_NODE_