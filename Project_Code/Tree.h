#ifndef _TREE_
#define _TREE_

#include <iostream>
#include "DoublyLinkedList.h"

class Tree
{
private:
	struct Tree_Node
	{
		int val;
		Tree_Node* right;
		Tree_Node* left;
		Tree_Node(int data);
		~Tree_Node();
	};

	Tree_Node* root;
	int num_of_nodes;

public:
	//Empty contractor
	Tree(const int& root_value);

	//Create binary search tree from array values.
	Tree(int* arr, int arr_size);

	//Destructor 
	~Tree();

	//Copy contractor
	Tree(const Tree& other);

	//Move contractor
	Tree(Tree&& other);

	//Assignment operator
	Tree& operator=(const Tree& other);

	//Move assignment operator
	Tree& operator=(Tree&& other);

	//Increment the num_of_nodes value
	void increment_tree_size();

	//Return the number of nodes in tree
	int get_num_of_nodes() const;

	//Return the root of the tree
	Tree::Tree_Node* get_root() const;

	//Insert new value to the BST.
	void insert_to_tree(Tree::Tree_Node* root, int val);

	//Return Inorder array of the Tree
	int* Inorder(Tree::Tree_Node* root, int* arr_size = 0) const;

	void print_Tree() const;

	//Create tree from the given array.
	void array_to_tree(int* arr, int arr_size);

	//Removes all the elements in the List
	void clear(Tree::Tree_Node* root);

	//Convert the given BST to List.
	Doubly_Linked_List& tree_to_list(Tree::Tree_Node* root);
};

#endif // !_TREE_