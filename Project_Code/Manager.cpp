#include "Manager.h"

Manager::~Manager()
{
	std::cout << "Delete Of All Workers." << std::endl;

	for (size_t i = 0; i < workers.size(); i++)
	{
		delete workers[i];
	}

}

void Manager::deepCopy(Manager* other)
{
	std::cout << "Manager - DeepCopy." << std::endl;
	this->workers.clear();

	auto ws = other->getWorkers();
	for (size_t i = 0; i < ws.size(); i++)
	{
		Worker* temp_worker = new Worker(ws[i]->getName(), ws[i]->getId());

		this->workers.push_back(temp_worker);
	}
}

void Manager::addWorker(std::string name, std::string id)
{

	workers.push_back(new Worker(name, id));

}

void Manager::addWorker(Worker* worker)
{
	workers.push_back(worker);
}

std::vector<Worker*> Manager::getWorkers()
{
	return this->workers;
}

