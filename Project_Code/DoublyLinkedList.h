#ifndef _LIST_
#define _LIST_

#include <iostream>
#include <string>
#include <vector>


/// <summary>
/// This class implemented a Doubly Linked List.
/// Each node consists of a data value, 
/// a pointer to the next node, and a pointer to the previous node.
/// https://en.wikipedia.org/wiki/Doubly_linked_list
/// </summary>
class Doubly_Linked_List
{
private:

    struct Node {
        int data;
        Node* next;
        Node* prev;
        Node(int value);
        ~Node();
    };

    Node* first_node;       // Points to the first item in the list
    Node* last_node;        // Points to the last item in the list
    int List_size;      // Number the elements in the list

public:

    //The constructor makes an initially empty list
    Doubly_Linked_List();

    //The constructor create list that have one Node.
    Doubly_Linked_List(const int& data);

    //The constructor create list that contain the array values
    Doubly_Linked_List(int* arr, int* size);

    //The destructor that reclaims the list's memory
    ~Doubly_Linked_List();

    //Copy constructor
    Doubly_Linked_List(const Doubly_Linked_List& other);

    //Move constructor
    Doubly_Linked_List(Doubly_Linked_List&& other);

    //Assignment operator
    Doubly_Linked_List& operator=(const Doubly_Linked_List& other);

    //Move assignment operator
    Doubly_Linked_List& operator=(Doubly_Linked_List&& other);

    //The method concatenate the given list to this list.
    Doubly_Linked_List& operator+(const Doubly_Linked_List& other);

    //Return true if the other list is equal to this list.
    bool operator==(const Doubly_Linked_List& other);
    
    //Return true if the other list is not equal to this list.
    bool operator!=(const Doubly_Linked_List& other);

    //Prints the contents of the linked list of integers.
    void print_list() const;

    //Returns the length of the linked list.
    int get_size() const;

    //Return the first Node.
    Doubly_Linked_List::Node* get_first_node() const;

    //Return the last Node.
    Doubly_Linked_List::Node* get_last_node() const;

    //Increment the size of the list.
    void increment_list_size();

    //Insert new value to the end of the list.
    void add_to_tail(int new_val);

    //Insert new value to the start of the list.
    void push_to_head(int new_val);

    //Sort the given List.
    void list_sort();

    //Remove the first Node that his data equal to value.
    //Return true if the value remove from the list, else return false.
    bool remove(int value);

    //Remove Nodes that contain the same data fields.
    void remove_duplicates();

    //Removes all the elements in the List.
    void clear();
};



#endif // !_LIST_