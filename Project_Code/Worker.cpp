#include "Worker.h"

Worker::~Worker()
{
	std::cout << "Destructor of Worker name: " << _name << std::endl;
}

Worker::Worker(Worker& other)
{
	std::cout << "Worker - Copy Constructor." << std::endl;
	_name = other._name;
	_id = other._id;
}

Worker::Worker(Worker&& other) 
{
	std::cout << "Worker - Move Constructor." << std::endl;

	_name = "";
	_id = "";

	std::swap(_name, other._name);
	std::swap(_id, other._id);
}

Worker& Worker::operator=(Worker& other)
{
	std::cout << "Worker - Copy assignment operator." << std::endl;

	if (&other == this)
		return *this;
	
	this->_name = other._name;
	this->_id = other._id;

	return *this;
}

Worker& Worker::operator=(Worker&& other)
{
	std::cout << "Worker - Move assignment operator." << std::endl;

	if (&other == this)
		return *this;

	this->_name = other._name;
	this->_id = other._id;

	other._name = "";
	other._id = nullptr;

	return *this;
}

std::string Worker::getName()
{
	return _name;
}

std::string Worker::getId()
{
	return _id;
}

std::ostream& operator<< (std::ostream& cout, const Worker& worker) {
	cout << "Worker Name: " << worker._name << std::endl;
	cout << "Worker ID: " << worker._id << std::endl;
	return cout;
}