#include "Stack.h"

Stack::~Stack()
{
	while (!isEmpty())
		pop();

	this->top = nullptr;
}

void Stack::push(int item)
{
	Node* n = new Node();
	n->val = item;
	n->next = top;
	top = n;

	if (min_stack == nullptr) {
		min_stack = new Stack();
		min_stack->top = new Node();
		min_stack->top->val = item;
		min_stack->top->next = nullptr;
	}
	else if (min_stack->top->val > item) {
		Node* n = new Node();
		n->val = item;
		n->next = min_stack->top;
		min_stack->top = n;
	}
}

void Stack::pop()
{
	if (isEmpty())
		return;

	Node* del = top;
	top = top->next;

	if (!stackIsSorted) {
		if (min_stack->top != nullptr && del->val == min_stack->top->val) {

			Node* toDell = min_stack->top;
			min_stack->top = min_stack->top->next;
			delete toDell;

		}
	}
	else {
		min_stack->top = min_stack->top->next;
	}

	delete del;
}

bool Stack::isEmpty()
{
	return top == nullptr;
}

int Stack::peek()
{
	return top->val;
}

int Stack::min()
{
	if (isEmpty())
		min_stack->push(std::numeric_limits<int>::max());
	return min_stack->peek();
}

void Stack::printStack() {
	std::cout << "The Stack Items:" << std::endl;
	std::cout << this;
	std::cout << std::endl;
}

void Stack::getSortStack()
{
	auto getLen = [](Stack* s) {
		int count = 0;
		Node* run = s->top;
		while (run != nullptr)
		{
			count++;
			run = run->next;
		}
		return count;
	};
	const int stackSize = getLen(this);

	if (stackSize == 0 || stackSize == 1)
		return;

	Node* h = this->top;
	for (size_t i = 0; i < stackSize; i++)
	{
		Node* run = h;
		Node* prev = run;
		run = run->next;
		for (size_t j = i + 1; j < stackSize; j++)
		{
			if (run->val < prev->val)
				std::swap(run->val, prev->val);
			prev = run;
			run = run->next;

		}
		h = h->next;
	}
	stackIsSorted = true;
	Node* temp = min_stack->top;
	min_stack->top = top;
	delete temp;
}


ostream& operator<<(ostream&, Stack* s) {
	Node* temp = s->top;

	while (temp != nullptr) {
		std::cout << "val = " << temp->val << std::endl;
		temp = temp->next;
	}

	return cout;
}