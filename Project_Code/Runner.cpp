﻿//Header For List and Trees Tests
void createListAndTrees();

//Header For Worker and Manager classes.
void create_WorkersAndManager();

//Header For Stack.
void createStack();

//Header For Queue.
void createQueue();

//Header For Car class.
void createCars();

//Header For LinkedList_Test.
void createLinkedList_Test();


int main() {

	createListAndTrees();

	create_WorkersAndManager();

	createStack();

	createQueue();

	createCars();

	createLinkedList_Test();

	return 0;
}