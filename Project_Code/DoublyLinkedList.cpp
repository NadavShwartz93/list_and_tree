#include "DoublyLinkedList.h"

Doubly_Linked_List::Doubly_Linked_List() : List_size(0), first_node(nullptr), last_node(nullptr)
{}

Doubly_Linked_List::Doubly_Linked_List(const int& data) : List_size(0), first_node(nullptr), last_node(nullptr)
{
	this->add_to_tail(data);
}

Doubly_Linked_List::Doubly_Linked_List(int* arr, int* size) : List_size(0), first_node(nullptr), last_node(nullptr)
{
	for (int i = 0; i < *size; i++)
	{
		this->add_to_tail(*arr);
		arr++;
	}
}


Doubly_Linked_List::Node::Node(int value) : data(value), next(nullptr), prev(nullptr) {
	std::cout << "Creating node " << data
		<< "  (" << reinterpret_cast<uintptr_t>(this) << ")\n";
}

Doubly_Linked_List::Node::~Node()
{
	std::cout << "Destroying node " << data
		<< "  (" << reinterpret_cast<uintptr_t>(this) << ")\n";
}

Doubly_Linked_List::~Doubly_Linked_List()
{
	this->clear();
}

//Copy constructor makes a copy of the other object's list.
Doubly_Linked_List::Doubly_Linked_List(const Doubly_Linked_List& other) : Doubly_Linked_List()
{
	//Walk through other's list inserting each of its elements
	//into this list
	Doubly_Linked_List::Node* head = other.first_node;
	for (int i = 0; i < other.get_size(); i++)
	{
		this->add_to_tail(head->data);
		head = head->next;
	}
}

//Move constructor takes possession of the temporary's list
Doubly_Linked_List::Doubly_Linked_List(Doubly_Linked_List&& other) : Doubly_Linked_List()
{
	//Swap contents with the temporary
	std::swap(this->first_node, other.first_node);
	std::swap(this->last_node, other.last_node);
	std::swap(this->List_size, other.List_size);
}

//Assignment operator
Doubly_Linked_List& Doubly_Linked_List::operator=(const Doubly_Linked_List& other)
{
	// Make a local, temporary copy of other
	Doubly_Linked_List temp{ other };
	// Exchange the head and tail pointers and len from this list
	// with those of the new, temporary list
	std::swap(this->first_node, temp.first_node);
	std::swap(this->last_node, temp.last_node);
	std::swap(this->List_size, temp.List_size);
	// The temporary list now points to this list's original contents, 
	// and this list now points to the copy of other's list
	// The temporary list will be destroyed since it is a temporary 
	return *this;
}

//Move assignment operator
Doubly_Linked_List& Doubly_Linked_List::operator=(Doubly_Linked_List&& other)
{
	// Exchange the head and tail pointers and len from this list
	// with those of the new, temporary list
	std::swap(this->first_node, other.first_node);
	std::swap(this->last_node, other.last_node);
	std::swap(this->List_size, other.List_size);
	// The temporary list now points to this list's original contents, 
	// and this list now points to the copy of other's list
	// The temporary list will be destroyed since it is a temporary 
	return *this;
}

Doubly_Linked_List& Doubly_Linked_List::operator+(const Doubly_Linked_List& other)
{
	Doubly_Linked_List::Node* head = other.first_node;
	for (int i = 0; i < other.get_size(); i++)
	{
		this->add_to_tail(head->data);
		head = head->next;
	}
	return *this;
}

bool Doubly_Linked_List::operator==(const Doubly_Linked_List& other)
{
	//The case the list is not in the same size.
	if (this->get_size() != other.get_size())
		return false;

	Doubly_Linked_List::Node* l1 = this->first_node;
	Doubly_Linked_List::Node* l2 = other.first_node;

	//Run over all the elements in the list and check if they are equal.
	for (int i = 0; i < this->get_size(); i++)
	{
		if (l1->data != l2->data)
			return false;

		l1 = l1->next;
		l2 = l2->next;
	}
	return true;
}

bool Doubly_Linked_List::operator!=(const Doubly_Linked_List& other)
{
	bool con = *this == other;
	if (con == false)
		return true;
	return false;
}

void Doubly_Linked_List::print_list() const
{
	Doubly_Linked_List::Node* head = this->first_node;
	for (int i = 0; i < this->get_size(); i++)
	{
		std::string str = std::to_string(i) + ". value: " + std::to_string(head->data);
		std::cout << str << std::endl;
		head = head->next;
	}
}

int Doubly_Linked_List::get_size() const
{
	return this->List_size;
}

void Doubly_Linked_List::increment_list_size()
{
	this->List_size = this->List_size + 1;
}

void Doubly_Linked_List::add_to_tail(int new_val)
{
	Doubly_Linked_List::Node* new_Node = new Node(new_val);
	this->increment_list_size();

	if (this->get_size() == 1) {
		this->last_node = this->first_node = new_Node;
	}
	else {

		//Update pointers
		this->last_node->next = new_Node;
		new_Node->prev = this->last_node;
		this->last_node = new_Node;
	}
}

void Doubly_Linked_List::push_to_head(int new_val)
{
	Doubly_Linked_List::Node* new_Node = new Node(new_val);
	this->increment_list_size();

	//Update pointers
	this->first_node->prev = new_Node;
	new_Node->next = this->first_node;
	this->first_node = new_Node;
}


bool Doubly_Linked_List::remove(int value)
{
	Doubly_Linked_List::Node* head = this->first_node;

	for (int i = 0; i < this->get_size(); i++)
	{
		if (head->data == value) {
			//The case the first Node data field equal to value.
			if (i == 0) {
				Doubly_Linked_List::Node* first = this->first_node;
				this->first_node = this->first_node->next;
				this->first_node->prev = nullptr;

				delete first;
			}
			//The case the last Node data field equal to value.
			else if (i + 1 == this->get_size()) {
				Doubly_Linked_List::Node* last = this->last_node;
				this->last_node = this->last_node->prev;
				this->last_node->next = nullptr;

				delete last;
			}
			else {
				Doubly_Linked_List::Node* temp = head;
				head->prev->next = head->next;
				head->next->prev = head->prev;

				delete temp;
			}
			this->List_size = this->List_size - 1;
			return true;
		}
		head = head->next;
	}
	return false;
}

void Doubly_Linked_List::remove_duplicates()
{
	std::vector<int> v;
	Doubly_Linked_List::Node* run = this->first_node;

	//Find the duplicates data.
	for (int i = 0; i < this->get_size(); i++)
	{
		Doubly_Linked_List::Node* head = run;
		while (head)
		{
			if (run->data == head->data)
				v.push_back(run->data);
			head = head->next;
		}
		run = run->next;
	}

	//Remove the duplicates Nodes.
	for (int i = 0; i < v.size(); i++)
	{
		this->remove(v[i]);
	}
}

void Doubly_Linked_List::clear()
{
	Doubly_Linked_List::Node* head = this->first_node;
	while (head)
	{
		auto temp = head;
		head = head->next;
		delete temp;
	}

	// Null head signifies list is empty
	this->first_node = this->last_node = nullptr;
	this->List_size = 0;
}

Doubly_Linked_List::Node* Doubly_Linked_List::get_first_node() const
{
	return this->first_node;
}

Doubly_Linked_List::Node* Doubly_Linked_List::get_last_node() const
{
	return last_node;
}


