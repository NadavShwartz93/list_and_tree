#ifndef _PERSON
#define _PERSON
#include <string>
#include <iostream>
#include <utility>

class Worker
{
private:
	std::string _name;
	std::string _id;
	
public:

	Worker(std::string name, std::string id) : _name(name), _id(id)
	{
		std::cout << "Constructor of Worker name: " << _name << std::endl;
	};

	Worker() : _name(""), _id("")
	{
		std::cout << "Constructor of Empty Worker" << std::endl;
	};


	virtual ~Worker();

	Worker(Worker& other);

	Worker(Worker&& other);

	Worker& operator=(Worker& other);

	Worker& operator=(Worker&& other);

	std::string getName();

	std::string getId();

	friend std::ostream& operator<< (std::ostream& cout, const Worker& workser);
};


#endif
