#include "Tree.h"

Tree::Tree_Node::Tree_Node(int data) : val(data), right(nullptr), left(nullptr)
{
    std::cout << "Creating node " << val
        << "  (" << reinterpret_cast<uintptr_t>(this) << ")\n";
}

Tree::Tree_Node::~Tree_Node()
{
    std::cout << "Destroying node " << val
        << "  (" << reinterpret_cast<uintptr_t>(this) << ")\n";
}


Tree::Tree(const int& root_value) : root(new Tree::Tree_Node(root_value)), num_of_nodes(1)
{}

Tree::Tree(int* arr, int arr_size) : root(new Tree::Tree_Node(*arr)), num_of_nodes(1)
{
    arr++;
    this->array_to_tree(arr, arr_size - 1);
}

void Tree::array_to_tree(int* arr, int arr_size) {
    for (int i = 1; i < arr_size; i++)
    {
        this->insert_to_tree(this->get_root(), *arr);
        arr++;
    }
}

Tree::~Tree()
{
    this->clear(this->get_root());
}

//Copy constructor makes a copy of the other object's tree.
Tree::Tree(const Tree& other) : Tree(other.root->val)
{
    int* arr = other.Inorder(other.get_root());
    this->array_to_tree(arr, other.get_num_of_nodes() - 1);
}

//Move constructor takes possession of the temporary's tree.
Tree::Tree(Tree&& other) : Tree(other.root->val)
{
    //Swap contents with the temporary.
    std::swap(this->root, other.root);
    std::swap(this->num_of_nodes, other.num_of_nodes);
}

//Assignment operator.
Tree& Tree::operator=(const Tree& other)
{
    //Make a local, temporary copy of other.
    Tree temp{ other };

    //Swap contents with the temporary.
    std::swap(this->root, temp.root);
    std::swap(this->num_of_nodes, temp.num_of_nodes);

    return *this;
}

//Move assignment operator.
Tree& Tree::operator=(Tree&& other)
{
    //Swap contents with the temporary.
    std::swap(this->root, other.root);
    std::swap(this->num_of_nodes, other.num_of_nodes);

    return *this;
}

void Tree::increment_tree_size()
{
    this->num_of_nodes = this->num_of_nodes + 1;
}

int Tree::get_num_of_nodes() const
{
    return this->num_of_nodes;
}

Tree::Tree_Node* Tree::get_root() const
{
    return this->root;
}


void Tree::insert_to_tree(Tree::Tree_Node* root, int val)
{
    if (root->left == nullptr && root->right == nullptr) {
        if (this->root->val > val)
            root->left = new Tree::Tree_Node(val);
        else if (this->root->val < val)
            root->right = new Tree::Tree_Node(val);
        this->increment_tree_size();
    }

    else if (this->root->val > val) {
        if(root->left != nullptr)
            this->insert_to_tree(root->left, val);
        else {
            root->left = new Tree::Tree_Node(val);
            this->increment_tree_size();
        }
    }

    else if (this->root->val < val) {
        if(root->right != nullptr)
            this->insert_to_tree(root->right, val);
        else {
            root->right = new Tree::Tree_Node(val);
            this->increment_tree_size();
        }
    }
}

//Return the inorder traversal of its nodes' values.
//inorder: Left, root, right.
int* Tree::Inorder(Tree::Tree_Node* root, int* arr_size) const
{
    if (this->root == nullptr)
        return nullptr;
    if (this->root->right == nullptr && this->root->left == nullptr) {
        int* arr = new int[1];
        *arr = this->root->val;
        (*arr_size)++;
        return arr;
    }
    else {
        int size_l = 0;
        int* l = Inorder(this->root->left, &size_l);

        int size_r = 0;
        int* r = Inorder(this->root->right, &size_r);

        (*arr_size) = size_l + size_r + 1;

        int* total = new int[*arr_size];

        memcpy(total, l, size_l * sizeof(int));
        total[size_l] = root->val;
        memcpy(total + size_l + 1, r, size_r * sizeof(int));

        return total;
    }

}

void Tree::print_Tree() const
{

}

void Tree::clear(Tree::Tree_Node* root)
{
    if (root != nullptr) {
        auto temp_root = root;
        this->clear(root->left);
        this->clear(root->right);
        delete temp_root;
    }
}

Doubly_Linked_List& Tree::tree_to_list(Tree::Tree_Node* root)
{
    if (root == nullptr) {
        Doubly_Linked_List* empty = new Doubly_Linked_List();
        return *empty;
    }
    if (root->left == NULL && root->right == NULL) {
        Doubly_Linked_List *l = new Doubly_Linked_List(root->val);
        return *l;
    }
    else {
        Doubly_Linked_List l = this->tree_to_list(root->left);
        Doubly_Linked_List r = this->tree_to_list(root->right);
        l.add_to_tail(root->val);

        l = l + r;
        return l;
    }
}
