#ifndef _STACK
#define _STACK

#include <iostream>
#include <limits>
#include "Node.h"
using namespace std;

class Stack {
public:

	Stack() : top(nullptr), min_stack(nullptr), stackIsSorted(false)
	{
	};

	~Stack();

	/// <summary>
	/// Add an item to the top of the stack.
	/// </summary>
	/// <param name="item">- the item to push to the stack.</param>
	void push(int item);

	/// <summary>
	/// Remove the top item from the stack.
	/// </summary>
	void pop();

	/// <summary>
	/// Return true if and only if the stack is empty. 
	/// </summary>
	/// <returns></returns>
	bool isEmpty();

	/// <summary>
	///  Return the top of the stack.
	/// </summary>
	/// <returns></returns>
	int peek();

	/// <summary>
	/// Return the minimum element in Stack.
	/// Th method complexity is O(1).
	/// </summary>
	/// <returns></returns>
	int min();

	/// <summary>
	/// Print The Stack Elements.
	/// </summary>
	void printStack();

	/// <summary>
	///  Method that sort the stack such that 
	///  the smallest items are on the top.
	/// </summary>
	void getSortStack();

private:
	Node* top;

	Stack* min_stack;

	bool stackIsSorted;

	friend ostream& operator<<(ostream&, Stack* s);
};

#endif // !_STACK