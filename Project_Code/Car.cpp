#include "Car.h"

#pragma warning(disable : 4996)

Car::Car(string color, string brand) : _brand(brand)
{
    _color = new char[color.length() + 1];
    strcpy(_color, color.c_str());
    std::cout << "Create Car class." << std::endl;
}

Car::~Car() {
    free(this->_color);
    std::cout << "Release Car class." << std::endl;
}

char* Car::get_color() {
    return _color;
}

string Car::get_brand() {
    return _brand;
}

void Car::set_color(char* color) {
    if (strlen(color) > strlen(this->_color)) {
        free(this->_color);
        this->_color = new char[strlen(color) + 1];
        strcpy(this->_color, color);
    }
    else
        this->_color = color;
}

void Car::set_brand(string brand) {
    this->_brand = brand;
}

Car::Car(Car& other) : _brand(other._brand)
{
    std::cout << "Copy Constructor of Car class." << std::endl;

    _color = new char[strlen(other._color) + 1];
    strcpy(_color, other._color);
}

Car::Car(Car&& other) : _brand("")
{
    std::cout << "Move Constructor of Car class." << std::endl;

    _color = other._color;
    other._color = nullptr;

    std::swap(other._brand, _brand);
}

Car& Car::operator=(Car& other)
{
    std::cout << "Copy Assignment of Car class." << std::endl;

    _color = new char[strlen(other._color) + 1];
    strcpy(_color, other._color);

    this->set_brand(other._brand);

    return *this;
}

Car& Car::operator=(Car&& other)
{
    std::cout << "Move Assignment of Car class." << std::endl;

    if (this == &other)
        return *this;

    _color = other._color;
    other._color = nullptr;

    this->set_brand("");
    std::swap(other._brand, this->_brand);

    return *this;
}