#include "Queue.h"

Queue::~Queue()
{
	while (!isEmpty())
		this->remove();

	this->head = nullptr;
	this->tail = nullptr;
}

void Queue::add(int item)
{
	Node* newNode = new Node();
	newNode->val = item;
	newNode->next = nullptr;

	//Queue is empty
	if (head == nullptr && tail == nullptr) {
		head = newNode;
		tail = head;
	}
	else
	{
		tail->next = newNode;
		tail = newNode;
	}

	//Increment the number of items in the Queue.
	numberOfItemsInQueue++;
}

void Queue::remove()
{
	if (isEmpty())
		return;

	Node* del = head;
	head = head->next;
	delete del;

	if (head == nullptr)
		tail = nullptr;

	//Decrement the number of items in Queue.
	numberOfItemsInQueue--;
}

Node* Queue::peek()
{
	return this->head;
}

bool Queue::isEmpty()
{
	return this->head == nullptr && this->tail == nullptr;
}

void Queue::printQueue() {
	std::cout << "The Queue Items:" << std::endl;
	std::cout << this;
	std::cout << std::endl;
}

int Queue::size() {
	return this->numberOfItemsInQueue;
}

void Queue::swap(Queue*& x)
{
	Node* tempHead = x->head;
	Node* tempTail = x->tail;

	x->head = this->head;
	x->tail = this->tail;

	this->head = tempHead;
	this->tail = tempTail;

	std::swap(this->numberOfItemsInQueue, x->numberOfItemsInQueue);
}

ostream& operator<<(ostream&, Queue* q) {

	Node* temp = q->head;

	while (temp != nullptr) {
		std::cout << "val = " << temp->val << std::endl;
		temp = temp->next;
	}

	return cout;
}
