#ifndef _QUEUE
#define _QUEUE

#include <iostream>
#include "Node.h"
using namespace std;

class Queue
{
public:

	Queue() : head(nullptr), tail(nullptr), numberOfItemsInQueue(0)
	{};

	~Queue();

	/// <summary>
	/// Add an item to the end of the list.
	/// </summary>
	void add(int item);

	/// <summary>
	/// Remove the first item in the list.
	/// </summary>
	void remove();

	/// <summary>
	/// Return the top of the queue.
	/// </summary>
	/// <returns></returns>
	Node* peek();

	/// <summary>
	/// Return true if and only if the queue is empty.
	/// </summary>
	/// <returns></returns>
	bool isEmpty();

	/// <summary>
	/// Print The Queue Elements.
	/// </summary>
	void printQueue();

	/// <summary>
	/// Return the size of the Queue.
	/// </summary>
	int size();

	void swap(Queue*& x);

private:
	Node* head;

	Node* tail;

	int numberOfItemsInQueue;

	friend ostream& operator<<(ostream&, Queue* q);
};

#endif // !_QUEUE

