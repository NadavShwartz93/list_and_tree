#ifndef _LINKED_LIST
#define _LINKED_LIST

#include <iostream>
#include <set>
using namespace std;

template <class T>
struct Node
{
	T val;
	Node* next;
};

/// <summary>
/// https://he.wikipedia.org/wiki/%D7%A8%D7%A9%D7%99%D7%9E%D7%94_%D7%9E%D7%A7%D7%95%D7%A9%D7%A8%D7%AA
/// </summary>
template <class T>
class LinkedList
{
public:
	LinkedList() : head(nullptr), numberOfItemsInList(0)
	{};

	~LinkedList();

	void insert(Node<T>* prev, T value);
	void insertBeginning(int value);
	void removeAfter(Node<T>* nodeToDelete);
	void removeBeginning();
	Node<T>* search(T value);
	void rotate_list();
	int size();
	bool isCyclic();
	Node<T>* getTheNthNode(int nodeNumber);

private:

	Node<T>* head;
	int numberOfItemsInList;
	friend std::ostream& operator<<(ostream&, LinkedList<T>* l);

};


template<typename T>
std::ostream& operator<<(ostream&, LinkedList<T>* l) {
	Node<T>* run = l->head;

	while (run != nullptr)
	{
		std::cout << "Value = " << run->val << std::endl;
		run = run->next;
	}

	delete run;
	return std::cout;
}

template<class T>
LinkedList<T>::~LinkedList()
{
	while (head != nullptr)
		removeBeginning();
}


template<class T>
void LinkedList<T>::insert(Node<T>* insertAfter, T value)
{
	Node<T>* newNode = new Node<T>();
	newNode->val = value;

	//If the List is empty there is not impact to the left argument.
	if (head == nullptr) {
		newNode->next = head;
		head = newNode;
		return;
	}

	Node<T>* run = head;
	while (run != nullptr)
	{
		if (run == insertAfter) {
			newNode->next = insertAfter->next;
			insertAfter->next = newNode;
		}
		run = run->next;
	}

	////Increment the number of items in the List.
	numberOfItemsInList++;
	delete run;
}

template<class T>
void LinkedList<T>::insertBeginning(int value)
{
	Node<T>* newNode = new Node<T>();
	newNode->val = value;
	newNode->next = head;
	head = newNode;

	////Increment the number of items in the List.
	numberOfItemsInList++;
}

template<class T>
void LinkedList<T>::removeAfter(Node<T>* nodeToDelete)
{
	//Decrement the number of items in List.
	numberOfItemsInList--;

	if (nodeToDelete == head) {
		head = head->next;
		delete nodeToDelete;
		return;
	}

	//The case that the nodeToDelete is not the first node in the list.
	Node<T>* run = head->next;
	Node<T>* prev = head;
	while (run != nullptr)
	{
		if (run == nodeToDelete) {
			prev->next = run->next;
			delete nodeToDelete;
		}
		prev = run;
		run = run->next;
	}

	delete run;
	delete prev;
}

template<class T>
void LinkedList<T>::removeBeginning()
{
	//Decrement the number of items in List.
	numberOfItemsInList--;

	if (head == nullptr)
		return;

	//The case that the head is not nullptr.
	Node<T>* toDelete = head;
	head = head->next;
	delete toDelete;
}

template<class T>
Node<T>* LinkedList<T>::search(T value)
{
	Node<T>* run = head;

	//Iterate to find the first node that it's val field is equal to value.
	while (run != nullptr && run->val != value) {
		run = run->next;
	}

	return run;
}

template<class T>
void LinkedList<T>::rotate_list()
{
	if (head == nullptr || head->next == nullptr)
		return;

	Node<T>* temp = head;
	Node<T>* temp2 = head->next;
	head->next = nullptr;

	while (temp2 != nullptr) {
		temp = temp2;
		temp2 = temp2->next;
		temp->next = head;
		head = temp;
	}
}

template<class T>
int LinkedList<T>::size()
{
	return numberOfItemsInList;
}

template<class T>
bool LinkedList<T>::isCyclic()
{
	if (this == nullptr || head->next == nullptr)
		return false;

	std::set<LinkedList*> s;
	Node<T>* runner = head;

	while (runner != nullptr) {
		if (s.count(runner) != 0) {
			delete runner;
			return true;
		}
		else {
			s.insert(runner);
			runner = runner->next;
		}
	}
	return false;
}

template<class T>
Node<T>* LinkedList<T>::getTheNthNode(int nodeNumber)
{
	int counter = 0;
	Node<T>* run = head;
	while (run != nullptr && counter < nodeNumber)
	{
		counter++;
		run = run->next;
	}
	return run;
}

#endif // !_LINKED_LIST