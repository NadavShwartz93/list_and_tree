#ifndef _CAR_
#define _CAR_

#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
using namespace std;

class Car {
private:
    char* _color;
    string _brand;
public:
    Car(string color, string brand);

    ~Car();

    Car(Car& other);

    Car(Car&& other);

    Car& operator=(Car& other);

    Car& operator=(Car&& other);

    char* get_color();

    string get_brand();

    void set_color(char* color);

    void set_brand(string brand);
};

#endif // !_CAR_