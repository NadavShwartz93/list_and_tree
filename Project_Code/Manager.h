#ifndef _MANAGER
#define _MANAGER

#include "Worker.h"
#include <vector>

class Manager : public Worker
{
private:

	std::vector<Worker*> workers;

public:

	Manager(std::string name, std::string id) : Worker(name, id)
	{
		std::cout << "Constructor of Manager name :" << name << std::endl;
	};

	~Manager();

	Manager(Manager& other);

	Manager(Manager&& other);

	Manager& operator=(Manager& other);

	Manager& operator=(Manager&& other);

	void addWorker(std::string name, std::string id);

	void addWorker(Worker* worker);

	std::vector<Worker*> getWorkers();

	void deepCopy(Manager* other);

};



#endif // !_MANAGER