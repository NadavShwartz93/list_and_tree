#include "../Project_Code/Worker.h"
#include "../Project_Code/Manager.h"

void create_Workers() {

	Worker k1("Nadav", "1111");
	std::cout << k1;
	Worker* K2 = new Worker("B", "2");
	Worker K3 = k1;
	Worker K4;
	K4 = k1;
	Worker K5 = std::move(Worker("C", "3"));

	delete K2;
}

void create_Manager() {
	Manager* m1 = new Manager("Nadav", "1");

	m1->addWorker("David", "11");
	m1->addWorker(new Worker("Ron", "12"));
	m1->addWorker(new Worker("Dan", "13"));

	Manager* m2 = new Manager("Shone", "2");
	m2->deepCopy(m1);

	delete m1;
	delete m2;
}

void create_WorkersAndManager() {

	std::cout << "Worker and Manager Tests:" << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;

	create_Workers();
	create_Manager();

	std::cout << "\nAll Tests in Worker and Manager PASSED." << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;
}