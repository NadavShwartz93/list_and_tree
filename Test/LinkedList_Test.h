#ifndef _LinkedList_Test_
#define _LinkedList_Test_

#include "../Project_Code/LinkedList.hpp"
#include <cassert>


class LinkedList_Test
{
public:
	LinkedList_Test();
	~LinkedList_Test();
	void run_LinkedList_Tests();

private:
	LinkedList<int>* l;

	bool Test_List_size_is_0();
	bool Test_List_searchNode();
	bool Test_2();
	bool Test_3();
	bool Test_4();
	bool Test_5();
	bool Test_6();
	bool Test_7();
};

#endif // !_LinkedList_Test_