#include "LinkedList_Test.h"

LinkedList_Test::LinkedList_Test()
{
	std::cout << std::endl << 
		"LinkedList_Test Tests\n----------------------------------------\n" <<
		std::endl;
	l = new LinkedList<int>();
	std::cout << "Create LinkedList_Test class." << std::endl;
};

LinkedList_Test::~LinkedList_Test() {
	std::cout << "Delete LinkedList_Test class." << std::endl;

	std::cout << std::endl <<
		"All Tests in Cars PASSED.\n---------------------------------------- " <<
		std::endl;
}

void LinkedList_Test::run_LinkedList_Tests()
{
	assert(this->Test_List_size_is_0() == true);
	assert(this->Test_List_searchNode() == true);
};

bool LinkedList_Test::Test_List_size_is_0()
{
	return this->l->size() == 0;
}

bool LinkedList_Test::Test_List_searchNode()
{
	l->insertBeginning(4 * 4);
	l->insertBeginning(4 * 4 + 1);
	Node<int>* n1 = l->getTheNthNode(0);
	return l->search(17) == n1;
}

void createLinkedList_Test() {
	LinkedList_Test* test = new LinkedList_Test();

	test->run_LinkedList_Tests();

	delete test;
}