#include "../Project_Code/Car.h"

void createCars() {

    std::cout << "Car Tests:" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    Car c1("Red", "BMW");

    Car c2 = c1;

    c2.set_color((char*)"Blue");
    std::cout << "The color of c2 car is: " << c2.get_color() << std::endl;
    std::cout << "The color of c1 car is: " << c1.get_color() << std::endl;

    Car c3(c1);
    c3 = c2;

    Car c4 = std::move(c1);
    c2 = std::move(c4);

    std::cout << "\nAll Tests in Cars PASSED." << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;
}