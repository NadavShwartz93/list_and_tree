#include "../Project_Code/Queue.h"
#include <cassert>

Queue* createQueue(int* arr, const int& size) {
	Queue* q = new Queue();
	for (size_t i = 0; i < size; i++)
	{
		q->add(arr[i]);
	}
	return q;
}

void checkSwap() {
	int arr1[] = { 1,2,3 };
	Queue* q1 = createQueue(arr1, sizeof(arr1) / sizeof(arr1[0]));

	int arr2[] = { 1,2,3,4,5 };
	Queue* q2 = createQueue(arr2, sizeof(arr2) / sizeof(arr2[0]));

	std::cout << "Before Swap\n";
	std::cout << "q1 size = " << q1->size() << std::endl;
	std::cout << "q2 size = " << q2->size() << std::endl;

	q1->swap(q2);

	assert(q1->size() == 5);
	assert(q2->size() == 3);

	std::cout << "After Swap\n";
	std::cout << "q1 size = " << q1->size() << std::endl;
	std::cout << "q2 size = " << q2->size() << std::endl;
}

void createQueue() {
	std::cout << "Queue Tests:" << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;

	int arr[] = { 1,2,3,-1 };

	Queue* q = new Queue();
	for (size_t i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		q->add(arr[i]);
	}

	q->printQueue();

	std::cout << "The number of items in Queue: " << q->size() << std::endl << std::endl;
	
	std::cout << "Check Queue Swap method:" << std::endl;
	checkSwap();
	std::cout << "Swap method test PASSED." << std::endl;

	delete q;

	std::cout << "\nAll Tests in Queue PASSED." << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;
}
