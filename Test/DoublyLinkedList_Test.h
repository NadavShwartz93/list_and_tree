#ifndef LIST_TEST
#define LIST_TEST

#include "../Project_Code/DoublyLinkedList.h"
#include <cassert>
#include <iostream>

class Doubly_Linked_List_Test
{
public:
	void run_List_Tests();
private:
	bool Test_List_size_is_0();
	bool Test_1();
	bool Test_2();
	bool Test_3();
	bool Test_4();
	bool Test_5();
	bool Test_6();
	bool Test_7();
};




#endif // !LIST_TEST
