#include "..\Project_Code\Stack.h"

void createStack()
{
	std::cout << "Stack Tests:" << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;

	int arr[] = { 1,2,3,-1 };

	Stack* s = new Stack();
	for (size_t i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		s->push(arr[i]);
	}
	s->printStack();

	std::cout << "The Minimum Element in Stack: " << s->min() << std::endl << std::endl;

	std::cout << "Sorted Stack..." << std::endl;
	s->getSortStack();
	s->printStack();

	delete s;

	std::cout << "\nAll Tests in Stack PASSED." << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;

}