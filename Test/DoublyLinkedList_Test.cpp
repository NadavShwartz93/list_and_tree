#include "DoublyLinkedList_Test.h"

void Doubly_Linked_List_Test::run_List_Tests() {
	assert(Test_List_size_is_0());
	assert(Test_1());
	assert(Test_2());
	assert(Test_3());
	assert(Test_4());
	assert(Test_5());
	assert(Test_6());
	assert(Test_7());
}

bool Doubly_Linked_List_Test::Test_List_size_is_0() {
	Doubly_Linked_List list;
	if (list.get_size() == 0)
		return true;
	return false;
}

//Check that the list have only 1 element, and the Node's data is equal to 3.
bool Doubly_Linked_List_Test::Test_1() {
	int data = 3;
	Doubly_Linked_List list(3);
	if (list.get_size() == 1 && list.get_first_node()->data == data)
		return true;
	return false;
}

//Check that all the elements in the array is in the list.
bool Doubly_Linked_List_Test::Test_2() {
	int array[] = { 1,2,3,4,5 };
	int arr_size = 5;

	Doubly_Linked_List list(array, &arr_size);

	auto head = list.get_first_node();

	for (int i = 0; i < arr_size; i++)
	{
		if (head->data != array[i])
			return false;
		head = head->next;
	}
	return true;
}

//The test using the Copy Constructor and check if the two list are equals.
bool Doubly_Linked_List_Test::Test_3()
{
	Doubly_Linked_List other(1);
	Doubly_Linked_List temp(other);

	if (temp == other)
		return true;
	return false;
}

//The test using the Move Constructor and check if the two list are not equals.
bool Doubly_Linked_List_Test::Test_4()
{
	Doubly_Linked_List l1(1);
	Doubly_Linked_List l2(2);
	Doubly_Linked_List concat = l1 + l2;
	Doubly_Linked_List temp = std::move(concat);

	if (temp != concat)
		return true;
	return false;
}

//Check Assignment Operator.
bool Doubly_Linked_List_Test::Test_5()
{
	Doubly_Linked_List l1, l2(4);
	l2 = l1;

	return l1 == l2;
}

//Move Assignment Operator.
bool Doubly_Linked_List_Test::Test_6()
{
	Doubly_Linked_List l1, l2(4);
	l2 = std::move(l1);

	return l1 != l2;
}

//Check add_to_tail method.
bool Doubly_Linked_List_Test::Test_7()
{
	Doubly_Linked_List tmp(13);
	tmp.add_to_tail(14);

	Doubly_Linked_List expec(tmp);


	return tmp == expec;
}

