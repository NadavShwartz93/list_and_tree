﻿#include <iostream>
#include "DoublyLinkedList_Test.h"

void createListAndTrees() {

	std::cout << "List and Tree Tests:" << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;
	Doubly_Linked_List_Test().run_List_Tests();
	std::cout << "\nAll Tests in List PASSED." << std::endl;
	std::cout << "----------------------------------------" << std::endl << std::endl;

}